Simple cluster running nginx pods and an Ingress resource balancing load.

1.- Create all the resources
> kubectl create -f files/
Note: ing.yaml file already has the secretName defined (certs). The secret can be
created retrospectively.

2.- Creating the certificates
> openssl req -newkey rsa:2048 -nodes -keyout tls.key -out tls.csr
> openssl x509 -in tls.csr -out tls.pem -req -signkey tls.key

3.- Creating secret from the files created previously
> kubectl create secret tls certs --cert tls.pem --key tls.key
------------------------------------------------------------------
It is possible to set up the Load Balancer manually for example to avoid HTTP traffic 
and only accept HTTPS.

It is necessary to pay attention to the following while creating the LB manually:
- Backends need to send the traffic to the open port by the service.
- Health check needs to check on this same port.
- Firewall rule needs to be creted to open this port.
- Certificates need to be copied and pasted manually.